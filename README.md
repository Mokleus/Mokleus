# Mokleus

Mokleus is an Exherbo Linux <https://exherbo.org> fork mainly designed to provide infrastracture handling for the Zernit project <https://git.dotya.ml/RXT0112/Zernit> with EOF phase set to be release for the community use.

### Abstract

Enoch build (ebuild) developed mainly for paludis <https://paludis.exherbo.org> designed to be:
- Cross-platform 
	- POSIX	IEEE Std 1003.1-2017 compatible
	- Paludis adapted to build on the target platforms

- Building from source with option to use pre-build packages for standard profiles
	- CD adapted to build the packages per profile
	- Profiles optimized to provide full packages that are designed to work on all specified systems (This may reduce the effectiveness of the package)
- Downstream adapted to allow publishing paludis-directory for public review.

- Proper abstracting
	- Exheres-0 is implemented poorly limiting the efficiency of CPP downstream while sacrificing on flexibility and portability that requires weird and hard to maintain helper functions alike https://gitlab.exherbo.org/exherbo/arbor/-/blob/master/packages/sys-devel/gcc/gcc.exlib#L140 to get cross-compilation working which is not acceptable for mokleus implementation as it's maintainance intensive

- Strict quality assurance for the downstream items
	- Exherbo Linux's exheres-0 ebuild for paludis requires constant debugging and "babysitting", this ebuild is expected to build every time else bug
	- Every ebuild has to pass CI and be peer-reviewed by X amount of project developers
	- Each package has to have the expected documentation so that the source code is understable to 2nd grader which is utilized as documentation downstream documentation

- Automatic version bumps
	- CI is adapted to automatically bump the package version in testing witin 30 minutes of upstream release else bug

- Community friendly
	- Exherbo Linux is designed to be hostile to it's users with the intention to allegedly scare away incompetent developers and users draining developer's resources by asking too many questions this is counter-productive and as such not implemented in Mokleus. Any relevant question is a valid and will be answered to the best of our ability, but the end-users are still considered developers encouraged to contribute as this distribution is depending on contributions to be sustainable.
	- Community is managed through community portals and encouraged to cooperate and contribute

- User friendly
	- Downstream is adapted to output helpful messages designed to be understandable by 2nd grader
	- Designed for end-users that do not have the required knowledge in Computer Science to understand how the distribution is working and why
		- Project will do it's best to provide the expected support with option to pay for priority support to make it economical and sustainable if needed.

- Designed for mission critical environment

- Multi-language
	- Downstream is adapted to output based on value stored in `$LANG` variable, contributions are indexed with `# FIXME-TRANSLATE`

- Adapted to use as many options as possible
	- Exheres-0 is designed to reduce the amount of used options that disallows to set package-specific options which is allowed here

- Any indentation and any code style
	- Exheres-0 requires indentation of 4 spaces while we are formatting the code through GNU Indent as a part of our CD thus you are allowed and encouraged to use your preferred indentations and code style assuming GNU indent being able to format your code in the expected indentation

- Editor IDE
	- IDE adapted to show hints related to the downstream