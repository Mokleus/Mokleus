-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

# Mokleus license

Version **29/10/2020-8601 | 18:56:32 CET**

Created by **Jacob Hrbek** ("Creator") identified with a GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> according to the keyserver <https://keys.openpgp.org> assuming these identifications not hijacked (if hijacked the rights in this license are considered as suspended until access is restored) licensed as all rights reserved unless explicitly stated otherwise in this license/files/directories for countries, but not limited to territories, planets, galaxies and whatever is behind, in front or in the middle of these that enforces copyright law (or it's equivalent) else the code is not allowed to be used under a sanction of waving fist at the general location of relevant jurisdiction area and screaming "Daaamn youu" while politely asking to respect the license as it is made to comply with four freedoms as defined by Free Software Foundation <https://en.wikipedia.org/wiki/Four_Freedoms_(software)> under a sanction granted by the relevant law.

As our ethiques are to be as transparent as possible you the affiliate parties with Mokleus ("this project") such as, but not limited to end-users, contributors, translators or simply as "you" are encouraged to ask any relevant question and expect an answer understandable by 2nd grader (else bug!) while having the full permission to change anything in this project assuming it following the abstract and passing quality assurance including a peer-review.

# 0. Terminology

Section dedicated to explaining used terms to avoid confusion as missunderstanding of this license is not an excuse to violating it. If you are not confident in understanding this license then you are requested to file a new issue tracking and as efficiently as possible to explain your issue for possible improvement.

### 0.1. FIXME flags

FIXME flags are for internal purposes i.e to mark a relevant area for implementation or reimplementation and thus it is not expected to be considered a part of the license.

FIXME flags are generally defined as:

```
FIXME: Something here
FIXME-LOCATOR: Something here
```

or as a comment which could be:

```
<!--
	FIXME: Something here

	something else here
- -->
```

END OF TERMINOLOGY

# 1. Conditions

Parties using or actively cooperating on this project are allowed to use the licensed content as provided in this license as long as they:
	1. Reference all authors of the software that you are using in case you decide to make a changes in the code.

	2. Keep the software and changes to the software open-source and allow general public to gain access to the source code on demand in clear, visible and intuitive way i.e using Gitea repository while providing help with the source code to the best of your ability limited by your resources and motivation.

	3. Their actions are not directly harmful to this project or it's community unless it is an act of self-defense or preservation of a life forms and it's species.
	
	4. Their actions are not violating this license.
	
	5. This license is not conflicting with laws in their jurisdictional area.
		5.1. In case this license is conflicting with law in their jurisdictional are they are required to inform the project maintainers about it and propose changes to use the licensed content legally.
	
	6. They are capable of reading and understanding this license.
		6.1. They have the option to file a new issue the relevant repository to request help and/or ask on hyperlink webchat.freenode.net/##law (requires Freenode account) which is community of volunteers willing to help them with the law.
		
	7. In case of a conflict you are expected to attempt solution through the original project for out-of-court resolution.

# 2. Rights of relevant parties
Life forms affiliated with this software usage and development have the following rights assuming conditions above not violated:
	1. Perform changes to the source code

	2. Redistribute the source code and it's binaries
	
	3. Politely request help with the source code assuming that your questions are in a language understandable by the party that you are requesting the help from and you are asking short, efficient and sensible questions while having the required knowledge to work with the source code
	
	4. Request more permission on demand in the issue tracker of the relevant project
		4.1. You are not allowed to directly perform changes in this license, but if you want to change something in this license you have the right to propose the change and implement it to the relevant repository once peer-reviewed assuming compilance with abstract of the repository in question.

# 3. Legality

This license is provided to the best of project's ability to not conflict with legality of relevant jurisdictional area which is humanly and theoretically impossible to comfort in the real time for reasons of, but not limited to insufficient space exploration, regular changes of the law for relevant areas and resources needed for the implementation.

As mentioned above you are not allowed to use the software if you are aware that it is conflicting with a law in your jurisdictional area.

# 4. Your agreement

You are not required to accept this license as you did not sign it, but nothing other then this license grants you the permission to use the copyrighted material the way it is described here and thus by using the software and/or utilizing the rights granted by this license and/or contributing to the project you indicate your acceptance of this license.

# 5. Maintenance of this license

This license is versioned at it's header using following format:

```sh
DD/MM/YYYY-ISO_STANDARD | HH:MM:SS TIMEZONE
```

with option to be encoded as `[DD][MM][YYYY][ISO_STANDARD][HH][MM][SS][0|1][TIMEZONE_DIFFERENCE]` where the `[0|1]` suffixing `TIMEZONE_DIFFERENCE` indicates either 0 as `+` (i.e. 050 meaning timezone +5) or 1 interpreted as `-` (i.e. 150 meaning timezone -5) in relation to UTC where example date `29/10/2020-8601 | 18:56:32 CEST` would look like:

```sh
291020208601185632001
```

and signed using a valid GPG signature of the copyright holder through software git <https://git-scm.com> which is designed to overwrite the previous version the day it has been merged in the original repository. The copyright holder reserves the right to perform the update at any time for any reason.

Each license update voids the previous license version.

# 6. Your contributions

Everyone is allowed to propose changes to the software as long as it complies with informations provided in `CONTRIBUTING.md` at the root of relevant repository (if it exists) and said life-form owns the copyright to the work that they are submitting to this project since submission of copyrighted material would make the project illegal in countries that enforce copyright law.

## 6.1. Requirement for signature
Your contributions has to be signed with a valid GPG signature to prove your copyright claim and to prove your acceptance of this license.

## 6.2. Right to revoke the contribution

In case the contributor's jurisdictional area grants them the right to revoke their contribution then by using a valid GPG signature they revoke the right to revoke their contribution for that specific contribution.

## 6.3. Right to be anonymous

In case the contributor contributing to the git repository wants to stay anonymous they can create a new **signed and unencrypted** GPG file in the `people/contributors/<NICKNAME_OR_OTHER_IDENTIFIER>` containing following:

```md
I the copyright holder identified as `<NICKNAME_OR_OTHER_IDENTIFIER>` here by transfer all the content that i've provided and/or will provide to the Mokleus project under the Mokleus license <LINK_TO_MOKLEUS_LICENSE> as a contributor.

I here by state that all of the content provided is my own and/or that i own the copyright to redistribute my changes that does not violate the Mokleus license.

I understand that submitting copyright conflicting material will result in my temporary suspension from the project for which i may be legally proscecuted by the relevant law depending on the decision of the copyright holder.
```

Or propose their own clause which may take longer to process as the Continuous Integration (CI) can't be adapted to allow auto-merges.

## 6.4. Patents

Contributors by using a valid GPG signature and submitting their material to this reymacs project grants the mentioned project a non-exclusive, space-wide, royalty-free patent license under the contributor's essential patent claims to make, use, sell, offer for sale, import and otherwise run, modify and propagate the provided material.

# 7. Warranty

This copyrighted material is provided by the copyright holder and affiliated parties with no warranty unless explicitly stated otherwise.

Should the mentioned material prove defective you as the user of the said material assume the cost of all necessary servicing, repair or corrections as a result of your usage of said material while having the obligation to inform the original project about this unwanted behavior so that it can be resolved.

If you are using said material and your party is not defined in this license then you assume a role of an end-user until your party can be appropriately defined.

# 8. Resolving of conflicts

In case you found a conflict in this license and/or in case the software is violating your rights and/or your wanted rights then you are expected to attempt resolution of this through issue tracking in the relevant project (if available) or through an electronic mail to the copyright holder prior to filing a lawsuit as this project is made to resolve these kinds of conflicts out-of-court.

## 9. Termination

Original project reserves the explicit right to terminate the rights of it's affiliated parties (i.e. End-users, Contributors, etc..) to this license in case they are in a jurisdictional area that enforces copyright law (or it's equivalent) that gives us the option/right to enforce this license and they break any of the stated text in this license.
The termination starts at immediately after the conflicting party has been informed about it though delivering this written text that is expected to be signed with valid GPG of the copyright holder or privileged party:

```md
## Mokleus license termination

Dear <NAME>,

We regret to inform you that your <project-name> rights to our license <LINK_TO_LICENSE> has been terminated for the following reason(s):

<REASON>

We demand that you resolve these issues as soon as possible so that your rights can be restored.

We are informing you that you have been granted special rights for this termination process that might be of use to you in case this termination conflicts with your culture or similar situation.

Sincerely,
- - <NAME>
```

In general project does it's best to avoid termination by providing a written warning so that the issues can be resolved appropriately which may be provided using a written text signed with a valid GPG signature of a copyright holder or privileged party:

```md
# Mokleus license violation warning

Dear <NAME>, your action is required,

We are informing you about what we believe is a license violation on your part:

<Explanation>

Thus the mokleus project demands that you resolve these issues within <VALUE> Hours otherwise we will be forced to terminate your rights to the copyrighted material provided by said project.

Thank you for your cooperation,
- - <NAME>
```

### 9.1. Rights in case of termination

We want to make the license acceptable for everyone and thus we grant these rights to the terminating party that are expected to make the license better in that area.

#### 9.1.1. Exceptions

Terminating party have the right to request exception which will be considered appropriately, for example cultural reasons are a valid reason for an exception that is then expected to be implemented in the license.

#### 9.1.2. Request for more time

In case the terminated party stated that they don't have the resources to resolve the mentioned issues in a specified time then they have the right to request more time.

#### 9.1.3. Request for help

In case the terminated party stated that they are not qualified to resolve these issues nor they don't have the resources to resolve them then they have the right to request help from the reymacs project.
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEdlrtMEIRwoQQ1cR4/LoEgrCrnxAFAl+bFHsACgkQ/LoEgrCr
nxCXrg//eTKgv97Hss78bs2Mj/tQ/7+YOSwi2+NPMG8N2yoIdPleAie/lYM+L66X
pVWTiz6Un/Gnszz6xFN0KFBEoEhRN6G7Z+6XMIX//DgTajwk4EgZ0QvFh9fD/hQ8
TYR4VtlEyuk1Tib3JlcCHp+RufSHfdwwAmmMP0xv6jMgqEuis+Y5gx175mBXz28j
kZg23tza7RAdKCRlZXpGUD4sHlKnJ7O/JSxeIYk15aY6KCZo1ge0v+WYMRt4dWlg
ZwfSHdF7dIPnNBBb5nQjwWwTHx2953ugVy9MSh3XeVqXZ8k7on4xNyfHkK2Fi1cG
pdF2m7694KqKum7ZuoyYH8Qf0P4jmxDQzlYcQkT6KIpwoEbROB1HGOBeJRoUolNG
wuvWDG2trtVte/zKZo6itLrBiNNITdSGtErFarucWW3EWM6HarJQuEOMlPhYJJ8H
nKOmUX6Oli74+q8qIJZf8keTMOTLYLF1gr+SnVbnXX9yLSpcRC3lk984Dsoi7Z2A
sK5AdXAufQNPh79iOGaBQxyt3yCoXkXR4rkwE7hNMTJkoBM+Rn9O2xIb4mHbCqLQ
of6NCJB0yKOoDXfonxuGJPzqBUJrjVDJu2cafhc6p/tcZQhUa2zZBMKOWrLgy6oQ
AZaPQcy5WU696BwEwbv3wY2VquAXSXdPMvtNMFZuOQqgH0XIlwE=
=n0G4
-----END PGP SIGNATURE-----
